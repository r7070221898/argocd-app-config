{{- if and (or (eq .Values.kind "Deployment") (eq .Values.kind "StatefulSet") (eq .Values.kind "DaemonSet")) .Values.containers  -}}
{{- $container_defaults := .Values.containerDefaults -}}
{{- $main_container_attrs := (mergeOverwrite (deepCopy $container_defaults) .Values.containers.main) -}}
{{- $main_container_readiness := (default $main_container_attrs.livenessProbe $main_container_attrs.readinessProbe) -}}
apiVersion: apps/v1
kind: {{ .Values.kind }}
metadata:
  name: {{ include "app.fullname" . }}
  annotations:
    {{- if .Values.svcPerPod }}
    service-per-pod-label: 'statefulset.kubernetes.io/pod-name'
    service-per-pod-ports: '80:{{ .Values.containers.main.port }}'
    {{- end }}
    {{- if .Values.reloadOnConfigmap }}
    configmap.reloader.stakater.com/reload: {{ .Values.reloadOnConfigmap }}
    {{- end }}
    {{ if .Values.deploymentAnnotations }}{{- toYaml .Values.deploymentAnnotations | nindent 4 }}{{ end }}
  labels:
    {{- include "app.labels" . | nindent 4 }}
spec:
  {{- if eq .Values.kind "Deployment" }}
  revisionHistoryLimit: 2
  progressDeadlineSeconds: {{ .Values.progressDeadlineSeconds }}
  minReadySeconds: {{ default (add (mul $main_container_readiness.periodSeconds $main_container_readiness.failureThreshold) (default 0 $main_container_readiness.initialDelaySeconds) 5) .Values.minReadySeconds }}
  strategy:
    {{- toYaml .Values.deploymentStrategy | nindent 4 }}
  {{- end }}
  {{- if or (eq .Values.kind "Deployment") (eq .Values.kind "StatefulSet") }}
  replicas: {{ .Values.replicas }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "app.selectorLabels" . | nindent 6 }}
  {{- if eq .Values.kind "StatefulSet" }}
  serviceName: {{ template "app.fullname" . }}-headless
  {{- end }}
  template:
    metadata:
      annotations:
        {{- if .Values.scalyr.enabled }}
        log.config.scalyr.com/include: {{ .Values.scalyr.enabled | quote }}
        log.config.scalyr.com/rename_logfile: {{ .Values.scalyr.log_prefix }}-{{ include "app.fullname" . }}
        log.config.scalyr.com/parser: app
        log.config.scalyr.com/attributes.parser: app
        {{- end }}
        {{ if .Values.podAnnotations }}{{- toYaml .Values.podAnnotations | nindent 8 }}{{ end }}
      labels:
        {{- include "app.selectorLabels" . | nindent 8 }}
    spec:
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      imagePullSecrets:
        {{- toYaml .Values.imagePullSecrets  | nindent 8 }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.serviceAccount.enabled }}
      serviceAccountName: {{ include "app.serviceAccountName" . }}
      {{- end }}
      containers:
        {{- $c_defaults := deepCopy .Values.containerDefaults }}
        {{- range $c_name, $c_attrs := deepCopy .Values.containers }}
        {{- $c_attrs := mergeOverwrite (deepCopy $c_defaults) (deepCopy $c_attrs) }}
        - name: {{ $c_name }}
          securityContext:
            {{- toYaml $c_attrs.securityContext | nindent 12 }}
          image: {{ $c_attrs.image }}
          imagePullPolicy: Always
          {{- if $c_attrs.workingDir }}
          workingDir: {{ $c_attrs.workingDir }}
          {{- end }}
          {{- if or $c_attrs.port $c_attrs.additionalPorts }}
          ports:
            {{- if $c_attrs.port }}
            - name: main
              containerPort: {{ $c_attrs.port }}
              protocol: TCP
            {{- end }}
            {{- if $c_attrs.additionalPorts }}
            {{- range $port := $c_attrs.additionalPorts }}
            - containerPort: {{ $port.port }}
              {{- if $port.name }}
              name: {{ $port.name }}
              {{- end }}
            {{- end }}
            {{- end }}
          {{- end }}
          env:
            {{- range $name, $value := $c_attrs.env }}
            - name: {{ $name }}
              {{- if typeIs "string" $value }}
              value: |-
                {{- toString $value | nindent 16 }}
              {{- else }}
              {{- toYaml $value | nindent 14 }}
              {{- end }}
            {{- end }}
          resources:
            limits:
              memory: '{{ $c_attrs.limits.memory }}Mi'
              {{ if $c_attrs.limits.cpu }}cpu: '{{ $c_attrs.limits.cpu }}m'{{ end }}
            requests:
              memory: '{{ div $c_attrs.limits.memory 2 }}Mi'
              {{ if $c_attrs.limits.cpu }}cpu: '{{ div $c_attrs.limits.cpu 2 }}m'{{ end }}
          command:
            {{ toYaml $c_attrs.command | nindent 12  }}
          args:
            {{ toYaml $c_attrs.args | nindent 12  }}
          {{- if or (hasKey $c_attrs.livenessProbe "exec") (hasKey $c_attrs.livenessProbe "httpGet") }}
          livenessProbe:
            {{- toYaml $c_attrs.livenessProbe | nindent 12 }}
          startupProbe:
            {{- toYaml (mergeOverwrite $c_attrs.livenessProbe $c_attrs.startupProbe) | nindent 12 }}
          {{- end }}
          {{- if or (hasKey $c_attrs.readinessProbe "exec") (hasKey $c_attrs.readinessProbe "httpGet") }}
          readinessProbe:
            {{- toYaml $c_attrs.readinessProbe | nindent 12 }}
          {{- end }}
          {{- if $.Values.volumes }}
          volumeMounts:
            {{- range $v_name, $v_attrs := $.Values.volumes }}
            - name: {{ $v_name }}
              mountPath: {{ (($v_attrs.attrs).mountPath) | default (($v_attrs.def.hostPath).path) }}
            {{- end }}
          {{- end }}
        {{- end }}
      nodeSelector:
        {{- toYaml .Values.nodeSelector | nindent 8 }}
      affinity:
        {{- toYaml .Values.affinity | nindent 8 }}
      tolerations:
        {{- toYaml .Values.tolerations | nindent 8 }}
      {{- if .Values.volumes }}
      volumes:
        {{- range $v_name, $v_attrs := .Values.volumes }}
        - name: {{ $v_name }}
          {{- if $v_attrs.def.configMap }}
          configMap:
            name: {{ include "app.fullname" $ }}-{{ $v_attrs.def.configMap.name }}
          {{- else }}
          {{- toYaml $v_attrs.def | nindent 10 }}
          {{- end }}
        {{- end }}
      {{- end }}
      {{- if .Values.customDNS.enabled }}
      dnsPolicy: "None"
      dnsConfig:
        nameservers:
          {{- toYaml .Values.customDNS.nameservers  | nindent 8 }}
        searches:
          {{- toYaml .Values.customDNS.searches  | nindent 8 }}
        options:
          - name: ndots
            value: "2"
          - name: edns0
      {{- end }}
{{- end }}
